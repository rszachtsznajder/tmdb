import angular from 'angular';

const theMovieDb = require('themoviedb-javascript-library');

class MovieDbApiService {
	constructor() {
		this.theMovieDb = theMovieDb;
		theMovieDb.common.api_key = 'ccf87e8e6756a77b6c086906c8013f3f';
	}

	search(text, page = 1) {
		return new Promise((resolve, reject) => {
			this.theMovieDb.search.getMovie({ query: encodeURIComponent(text), page }, resolve, reject);
		}).then(result => angular.fromJson(result));
	}

	getImage(size, file) {
		return this.theMovieDb.common.getImage({ size, file });
	}
}

export default MovieDbApiService;
