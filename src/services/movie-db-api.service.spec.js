import MovieDbApiService from './movie-db-api.service';

describe('MovieDbApiService', () => {
	let service;
	let theMovieDbMock;

	beforeEach(() => {
		theMovieDbMock = {
			common: jasmine.createSpyObj('theMovieDb.common', ['getImage']),
			search: jasmine.createSpyObj('theMovieDb.search', ['getMovie'])
		};

		service = new MovieDbApiService();
		service.theMovieDb = theMovieDbMock;
	});

	describe('search() method', () => {
		it('should call API\'s getMovie method and return Promise', () => {
			// given
			const text = 'foo bar';
			const expectedQuery = 'foo%20bar';
			const page = 2;

			// when
			const result = service.search(text, page);

			// then
			expect(theMovieDbMock.search.getMovie)
				.toHaveBeenCalledWith({ query: expectedQuery, page }, jasmine.any(Function), jasmine.any(Function));
			expect(result instanceof Promise).toBeTruthy();
		});

		it('should call for page 1 if it wasn\'t set', () => {
			// when
			service.search('');

			// then
			expect(theMovieDbMock.search.getMovie.calls.argsFor(0)[0]).toEqual(jasmine.objectContaining({ page: 1 }));
		});

		it('should resolve promise to object in form according to returned json', () => {
			// given
			const text = 'foo bar';
			const page = 2;
			theMovieDbMock.search.getMovie.and.returnValue('{"total":2,"results":[{"title":"a"},{"title":"b"}]}');

			// when
			const promise = service.search(text, page);

			// then
			promise.then((result) => {
				expect(result).toEqual({
					total: 2,
					results: [{ title: 'a' }, { title: 'b' }]
				});
			});
		});
	});

	it('should call API\'s getImage method on getImage() call', () => {
		// given
		const size = 'w500';
		const file = 'foo.jpg';

		// when
		service.getImage(size, file);

		// then
		expect(theMovieDbMock.common.getImage).toHaveBeenCalledWith({ size, file });
	});
});
