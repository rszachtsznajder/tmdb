import MoviesController from './movies.controller';

describe('MoviesController', () => {
	let controller;
	let scopeMock;
	let routeParamsMock;
	let MovieDbApiMock;

	beforeEach(() => {
		scopeMock = jasmine.createSpyObj('$scope', ['$apply']);
		routeParamsMock = {};
		MovieDbApiMock = jasmine.createSpyObj('MovieDbApi', ['search', 'getImage']);
	});

	beforeEach(() => {
		controller = new MoviesController(scopeMock, routeParamsMock, MovieDbApiMock);
	});

	describe('$onInit method', () => {
		let query;
		let page;

		beforeEach(() => {
			spyOn(controller, 'search');

			query = 'foo bar';
			page = 2;
			routeParamsMock.query = query;
			routeParamsMock.page = page;
		});

		it('should call search service with appropriate properties', () => {
			// when
			controller.$onInit();

			// then
			expect(controller.search).toHaveBeenCalledWith(query, page);
		});

		it('should call search with page 1 by default', () => {
			// given
			page = 1;
			delete routeParamsMock.page;

			// when
			controller.$onInit();

			// then
			expect(controller.search).toHaveBeenCalledWith(query, page);
		});
	});

	describe('search method', () => {
		let query;
		let page;

		beforeEach(() => {
			query = 'foo bar';
			page = 2;
			MovieDbApiMock.search.and.returnValue(Promise.resolve({}));
		});

		it('should prepare search results', () => {
			// given
			const apiResponse = {
				page: 2,
				results: [{ title: 'a' }, { title: 'b' }],
				total_pages: 3,
				total_results: 100
			};
			MovieDbApiMock.search.and.returnValue(Promise.resolve(apiResponse));

			// when
			const promise = controller.search(query, page);

			// then
			promise.then(() => {
				expect(controller.movies).toEqual([{ title: 'a' }, { title: 'b' }]);
				expect(controller.page).toEqual(2);
				expect(controller.totalPages).toEqual(3);
				expect(controller.totalResults).toEqual(100);
				expect(controller.pages).toEqual([1, 2, 3]);
			});
		});

		it('should call $scope.$apply function', () => {
			// when
			const promise = controller.search(query, page);

			// then
			promise.then(() => {
				expect(scopeMock.$apply).toHaveBeenCalled();
			});
		});
	});

	describe('getImage method', () => {
		let path;

		beforeEach(() => {
			path = 'foo.jpg';
		});

		it('should return the result of service getImage method call', () => {
			// given
			const imagePath = '/path/to/image.jpg';
			MovieDbApiMock.getImage.and.returnValue(imagePath);

			// when
			const result = controller.getImage(path);

			// then
			expect(result).toEqual(imagePath);
		});

		it('should return an empty string if given path is empty', () => {
			// given
			path = null;
			const imagePath = '';

			// when
			const result = controller.getImage(path);

			// then
			expect(result).toEqual(imagePath);
		});
	});

	describe('getMovieYear method', () => {
		let movie;
		let year;

		it('should return year based on release_date', () => {
			// given
			movie = { release_date: '2010-12-14' };
			year = '2010';

			// when
			const result = controller.getMovieYear(movie);

			// then
			expect(result).toEqual(year);
		});

		it('should return a "?" string if release date is not in proper format', () => {
			// given
			movie = { release_date: '2010/12/14' };
			year = '?';

			// when
			const result = controller.getMovieYear(movie);

			// then
			expect(result).toEqual(year);
		});

		it('should return a "?" string if release date is not given', () => {
			// given
			movie = {};
			year = '?';

			// when
			const result = controller.getMovieYear(movie);

			// then
			expect(result).toEqual(year);
		});
	});
});
