import MoviesController from './movies.controller';

const templateHTML = require('./movies.tpl.html');

require('./movies.scss');

const MoviesComponent = {
	controller: MoviesController,
	controllerAs: 'vm',
	template: templateHTML
};

export default MoviesComponent;
