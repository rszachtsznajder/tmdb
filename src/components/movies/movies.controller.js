import _ from 'lodash';

class MoviesController {
	constructor($scope, $routeParams, MovieDbApi) {
		this.$scope = $scope;
		this.$routeParams = $routeParams;
		this.MovieDbApi = MovieDbApi;
	}

	$onInit() {
		this.query = this.$routeParams.query;
		const page = _.get(this.$routeParams, 'page', 1);

		this.search(this.query, page);
	}

	search(query, page) {
		return this.MovieDbApi.search(query, page)
			.then(this._prepareSearchResult.bind(this))
			.then(() => {
				this.pages = _.range(1, this.totalPages + 1);
			})
			.then(this.$scope.$apply.bind(this.$scope));
	}

	getImage(path) {
		if (!path) {
			return '';
		}

		return this.MovieDbApi.getImage('w500', path);
	}

	getMovieYear(movie) {
		const dateMatch = _.get(movie, 'release_date', '').match(/^(\d{4})-\d{2}-\d{2}$/);

		if (dateMatch) {
			return dateMatch[1];
		}

		return '?';
	}

	_prepareSearchResult(searchResult) {
		this.movies = searchResult.results;
		this.page = searchResult.page;
		this.totalPages = searchResult.total_pages;
		this.totalResults = searchResult.total_results;
	}
}

export default MoviesController;
