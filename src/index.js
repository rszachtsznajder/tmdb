import angular from 'angular';
import 'angular-route';
import 'bootstrap';

import AppController from './controllers/app';
import MoviesComponent from './components/movies/movies.component';
import MovieDbApiService from './services/movie-db-api.service';

import './theme/main.scss';

angular.module('tmdbApp', ['ngRoute'])
	.controller('AppController', AppController)
	.component('movies', MoviesComponent)
	.service('MovieDbApi', MovieDbApiService)
	.config(($routeProvider) => {
		$routeProvider
			.when('/', {
				template: '<small>Use search form above to find a movie.</small>'
			})
			.when('/search/:query/:page?', {
				template: '<movies></movies>'
			})
			.when('/movie/:id/:title', {
				template: 'movie preview'
			})
			.otherwise('/');
	});
