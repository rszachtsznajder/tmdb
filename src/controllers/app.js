const AppController = ($scope, $location) => {
	$scope.submit = () => {
		const query = encodeURIComponent($scope.searchText);
		$location.url(`/search/${query}`);
	};
};

export default AppController;
