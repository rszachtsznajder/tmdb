import AppController from './app';

describe('AppController', () => {
	let scopeMock;
	let locationMock;

	beforeEach(() => {
		scopeMock = {};
		locationMock = jasmine.createSpyObj('$location', ['url']);
	});

	beforeEach(() => {
		AppController(scopeMock, locationMock);
	});

	it('should define submit method on angular scope', () => {
		expect(scopeMock.submit).toEqual(jasmine.any(Function));
	});

	it('should change location URL on submit() call', () => {
		// given
		scopeMock.searchText = 'foo bar';
		const expectedUrl = '/search/foo%20bar';

		// when
		scopeMock.submit();

		// then
		expect(locationMock.url).toHaveBeenCalledWith(expectedUrl);
	});
});
