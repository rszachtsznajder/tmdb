# README #

TMDB is an app that was done as a part of recruitment task.

### How do I get set up? ###

First of all you need to install [Node.js](https://nodejs.org/en/download/) with npm tool. 

Then in main project directory call `npm install` command. This should install all dependencies needed by project.

#### Development server ####

In `package.json` file is `start` script defined. When you call `npm run start` in main directory, the webpack tool will build the project and run it on development server. 
Read the result of the command in terminal to find correct URL of running server.

#### Project build ####

If you run `npm run build` script, the `dist` directory will be created. In this directory you will be able to find all files that should be copied to HTTP server on final project evironment.

#### Test ####

Command `npm run test` may be used to execute all tests available in the project.
