const devConfig = require('./webpack.config.js');

delete devConfig.entry;
devConfig.devtool = 'eval';

module.exports = devConfig;
