const path = require('path');
const webpack = require('webpack');

const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	entry: './src/index.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, '..', 'dist')
	},
	devtool: 'cheap-eval-source-map',
	plugins: [
		new CopyWebpackPlugin([{
			from: 'index.html',
			to: path.resolve(__dirname, '..', 'dist', 'index.html')
		}]),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default']
		}),
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				use: ['eslint-loader'],
				exclude: /node_modules/,
				enforce: 'pre'
			},
			{
				test: /\.js$/,
				use: ['babel-loader'],
				include: [
					path.resolve(__dirname, '..', 'src')
				]
			},
			{
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', 'sass-loader']
			},
			{
				test: /\.tpl\.html$/,
				exclude: /node_modules/,
				use: ['raw-loader']
			}
		]
	}
};